<?php


class Persona {

    public $nombre;
    public $apellidos;
    public $altura;
    public $edad;

    public function getNombre(){
        return $this->nombre;
    }

    public function getApellidos(){
        return $this->apellidos;
    }

    public function getAltura(){
        return $this->altura;
    }

    public function getEdad(){
        return $this->edad;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setApellidos($apellidos){
        $this->apellidos = $apellidos;
    }

    public function setAltura($altura){
        $this->altura = $altura;
    }

    public function setEdad($edad){
        $this->edad = $edad;
    }

    public function hablar(){
        return "Estoy hablando";
    }

    public function caminar(){
        return "Estoy caminando";
    }
}


class Informatico extends Persona{

    public $lenguajes;
    public $experienciaProgramando;

    public function __construct(){
        $this->lenguajes = "html, css y js";
        $this->experienciaProgramando = 10;
    }

    public function sabeLenguajes($lenguajes){
        $this->lenguajes = $lenguajes;

        return $this->lenguajes;
    }

    public function programar(){
        return "Soy programador";
    }

    public function repararPc(){
        return "Reparando pc";
    } 

    public function programarEnVue(){
        return "Programo en Vue.js";
    }
}

class QA extends Informatico{
    public $auditarSoftware;
    public $pruebasAutomatizadas;

    public function __construct(){
        parent::__construct(); //sino llamo al constructor parent no heredará las propiedades seteadas en la clase padre
        //el doble punto sirve para acceder a un método de forma estática sin tener que instanciar el objeto.
        $this->auditarSoftware = 'experto';
        $this->pruebasAutomatizadas = 5;
    }

    public function auditoria(){
        return "Estoy auditando un software";
    }
}