<?php

//*********************************************
//*******************EJEMPLO 1*****************
//*********************************************

// function devuelveElNombre($nombre, $apellidos){
//     $texto = "El nombre es: $nombre"
//             ."<br/>".
//             "Los apellidos son: $apellidos";

//     return $texto;
// }

// echo devuelveElNombre ("jose", "Arevalo");

//*********************************************
//*******************EJEMPLO 2*****************
//*********************************************

function getNombre($nombre){
    $texto = "El nombre es: $nombre";
    return $texto;
}

function getApeliidos ($apellidos){
    $texto = "Los apellidos son: $apellidos";
    return $texto;
}

function devuelveElNombre($nombre, $apellidos){
    $texto = getNombre($nombre)
            ."<br/>".
            getApeliidos($apellidos);

    return $texto;
}

echo devuelveElNombre ("Jose", "Arevalo");
echo getNombre("Gilbert");