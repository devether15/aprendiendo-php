<?php

/*
Variables locales: Son las que se definen dentro de una 
función  y no puden ser usadas fuera de la función,
solo están disponibles adentro. A no ser que hagamos
un return

Variables globales: Son las que se declaran fuera de 
una función y están disponibles dentro y fuera de las funciones
*/
//*********************************************
//*******************EJEMPLO 1*****************
//*********************************************

// de esta forma la variable echo no puede ser accedida
//desde afuera de la función porque no tiene rerturn

// $frase = "Ser es hacer";

// echo $frase;

// function mostrarFrase(){
// global $frase;

//     echo "<h1>$frase</h1>";

//     $year = 2019;
//     echo "<h1>$year</h1>";
// }

// mostrarFrase();

// echo $year;

//*********************************************
//*******************EJEMPLO 2*****************
//*********************************************

//De esta forma si se pude acceder a la variable 
//$year desde afuera porque posee return adentro y
//un echo desde la invocación afuera de la función

//en el caso de $frase, funciona porque se usa 
//adentro de la función la palabra global.

$frase = "Ser es hacer";

//echo $frase;

function mostrarFrase(){
global $frase;

    echo "<h1>$frase</h1>";

    $year = 2019;  
    
    return $year;
}

echo mostrarFrase();
