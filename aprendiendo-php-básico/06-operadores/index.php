<?php

//operadores aritméticos

$numero1 = 55;
$numero2 = 33;

echo 'Suma: '.($numero1+$numero2).'<br/>';
echo 'Resta: '.($numero1-$numero2).'<br/>';
echo 'multiplicación: '.($numero1*$numero2).'<br/>';
echo 'Divición: '.($numero1/$numero2).'<br/>';
echo 'Modulo: '.($numero1%$numero2);

//Clase 31: Operadores de incremento o decremento;
$year = 2019;

//Incremento
//$year + 1;
$year++;

echo "<h1>$year</h1>";
//Decremento
//$year - 1;
$year--;

//Pre - incremento
//$year = 1 + $year;
++$year;

//Pre - decremento
//$year = 1 - $year;
--$year;

echo "<h1>$year</h1>";

//Clase 32: Operadores de asignacion
$edad = 55;

echo $edad.'<br/>';


//$edad = $edad = 5;
echo ($edad+=5).'<br/>';
echo ($edad-=5).'<br/>';
echo ($edad*=5).'<br/>';
echo ($edad/=5).'<br/>';

?>


