<?php

//Buclee while

/*
   Es una estructura de control que itera o repidte la ejecucion 
   de una serie de insruccions tantas veces como sea necesario 
   en base a una condicion
 
 while(condicion){
      bloque de instruciones
      Otra instruccion 
 }
 
  */

$numero = 0;
 while ($numero <= 100) {
     echo "$numero";
     if ($numero != 100) {
         echo ", ";
     }
     $numero ++;
    
}

echo "<hr>";

//EJemplo tabla de multiplicar de un paramtero recibido por GET

if (isset($_GET['numero'])) {
    //cambiar tipo de dato (casteo)
    $numero = (int) $_GET['numero'];
} else {
    $numero = 1;
}

//var_dump($numero);

echo "<h1>tabla de multiplicar del número $numero</h1>";

$contador = 0;
while ($contador <= 10) {
    echo "$numero x $contador = ".($numero*$contador)."<br>"; 
    $contador ++;
    
}
//DO WHILE
//es igual que el while pero la condicion se evalua al final, por tanto el codigo se ejecuta aunque sea una vez
/*
do{
    //bloque de intrucciones
}while(condición);

*/

$edad = 17;
$contador = 1;

do{
    echo "Tienes acceso al local privado $contador <br/>";
    $contador ++; 
}while($edad >= 18 && $contador <= 10);

?>