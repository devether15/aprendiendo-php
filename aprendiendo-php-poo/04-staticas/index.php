<?php

require_once 'configuration.php';

Configuration::setColor('Azul');
Configuration::setEntorno('localhost');
Configuration::setNewsletter(true);

echo Configuration::$color.'<br>';
echo Configuration::$newsletter.'<br>';
echo Configuration::$entorno.'<br>';

echo '<hr>';

$config = new Configuration();
$config::$color = 'rojo';
echo $config::$color.'<br>';
echo $config->$color.'<br>';//esta forma de acceder no funciona porque es método estatico. se debe usar self::

var_dump($config);