<?php

require_once 'Coche.php';

$coche = new Coche("Azul", "renault", "logan", 150, 200, 4);
$coche2 = new Coche("verde", "subaru", "impreza", 180, 250, 4);
$coche3 = new Coche("rojo", "ford", "focus", 140, 100, 4);
$coche4 = new Coche("blanco", "honda", "civic", 200, 300, 2);

var_dump($coche).'<hr>';
var_dump($coche2).'<hr>';
var_dump($coche3).'<hr>';
var_dump($coche4).'<hr>';
var_dump($coche4->getColor()).'<hr>';

$coche->setColor('negro').'<hr>';
echo $coche->color.'<hr>';

echo '<h2>practica sobre la visibilidad (clase 222)</h2>'.'<br>';

$coche->color = 'Rosa';
$coche->setModelo('Twingo'); //cuando la propiedad es protected no puedo modificarla fuera de la clases, necesito un método para setearla
var_dump($coche);
echo '<hr>';

var_dump($coche->getMarca()); //para obtener o modificar una propiedad privada también necesito hacerlo a través de un método público en la clase
echo '<hr>';


echo '<h2>practica sobre tipado (clase 223)</h2>'.'<br>';

echo $coche->mostrarInfo($coche2);
//echo $coche->mostrarInfo("hola");
