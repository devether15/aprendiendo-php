<?php

//programacion Orientada a Objetos en PHP (POO)

//Definir una clase

//Es un molde para crear más objetos del tipo Coche con caracteristicas parecidas

class Coche{
    //atributos o propiedades

    // PUBLIC: Podemos acceder desde cuyuqluier lugar, dentro de esta clase
    // actual, dentro de clases que heredan de esta clase o fuera de la clase

    // PROTECTED: Podemos acceder desde la clase que los define y de clases que hereden
    // de esta clase

    // PRIVATE: Unicamente se pueden acceder desde esta clase

    public $color;
    protected $modelo;
    private $marca; 
    public $velocidad; 
    public $caballaje; 
    public $plazas;

    public function __construct($color, $modelo, $marca, $velocidad, $caballaje, $plazas){
        $this->color = $color;
        $this->modelo = $modelo;
        $this->marca = $marca;
        $this->velocidad = $velocidad;
        $this->caballaje = $caballaje;
        $this->plazas = $plazas;
    }

    //metodos (antes funciones) Son funciones que hace el objeto, (su comportamiento)

    public function getColor(){
        //el $this busca en esta clase la propiedad indicada
        return $this->color;
    }

    public function setColor($color){
        $this->color = $color;
    }

    public function setModelo($modelo){
        $this->modelo = $modelo;
    }

    public function getModelo(){
        return $this->modelo;
    }

    public function setMarca($marca){
        $this->marca = $marca;
    }

    public function getMarca(){
        return $this->marca;
    }    

    public function acelerar(){
        $this->velocidad++;
    }

    public function frenar(){
        $this->velocidad--;
    }

    public function getVelocidad(){
        return $this->velocidad;
    }

    public function mostrarInfo(Coche $miObjeto){ //acá puedo especificarle el tipo de dato que necesita recibir, sino paso el mismo falla

        if(is_object($miObjeto)){
            $info = '<h2>Información del coche</h2>';
            $info .= 'Modelo: '.$miObjeto->modelo.'<br>';
            $info .= 'Color: '.$miObjeto->color.'<br>';
            $info .= 'Velocidad: '.$miObjeto->velocidad;
        }else{
            $info = "Tu dato es este: $miObjeto";
        }

        return $info;
    }

} // fin definicion de la clase


