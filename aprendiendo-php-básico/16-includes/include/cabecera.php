<!DOCTYPE HTML>

<html lang ="es">
    <head>
        <meta charset="utf-8" />
        <title>Includes en PharException</title>
    </head>
    <body>
    <?php
        $nombre = "José Arévalo";
    ?>
        <!--Cabecera-->
        <div class="cabecera">
            <h1>Includes en PHP</h1>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="sobremi.php">Sobre Mi</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li><a href="https://google.es">Google</a></li>
            </ul>
            <hr/>
        </div>