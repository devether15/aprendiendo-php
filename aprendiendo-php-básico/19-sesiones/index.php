<?php

/*
Sesión: Almacena y persiste datos del usuario mientras que navega
en un sitio web hasta que cierra sesión o cierra el navegador
*/

//Iniciar sesión
session_start();

//variable local
$variable_normal = "soy una cadena de etxto";

//variable de sesión
$_SESSION['variable_persistente'] = "HOLA SOY UNA SESIÓN ACTIVA";

echo $variable_normal."<br/>";
echo $_SESSION['variable_persistente'];

/*
para probar este ejemplo, acceder al index: http://localhost/aprendiendo-php/19-sesiones/index.php
en este fichero hay una sesión iniciada, luego ir a la url de pagina1.php
ahí seguirá existiendo la sesión ya que también cuenta con el método 
session_start(); 

Continuamos yendo al logout.php, en este archivo está declarado el método
session_destroy(); por tanto cuando regresemos a pagina1.php, no habrá información
disponible ya que la sesión fue cerrada, pero si finalmente regresamos a index.php
se iniciará la sesión nuevamente
*/

//Mejora: utilizar includes en los ficheros para hacer la navegación más fácil 
// y no repetir código

?>

<ul>
    <li><a href="index.php">Index</a></li>
    <li><a href="pagina1.php">Pagina1</a></li>
    <li><a href="logout.php">logout</a></li>
</ul>