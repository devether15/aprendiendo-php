<?php

$cantantes = ['2pac', 'Drake', 'J Lo', 'Anathema'];
$numeros = [1,2,6,5,8,7,3,4];



//ordenamiento alfabeticamente
asort($cantantes);
var_dump($cantantes);
echo '<hr>';

//ordenamiento inverso
arsort($cantantes);
var_dump($cantantes);
echo '<hr>';

//ejemplo con numeros
asort($numeros);
var_dump($numeros);
echo '<hr>';

//añadir elementos al array
$cantantes[] = "Shakira";
var_dump($cantantes);
echo '<hr>';

//método que agrega el nuevo elemento al final del arreglo 
array_push($cantantes, "Metallica");
var_dump($cantantes);
echo '<hr>';

//eliminar último elemento del array
array_pop($cantantes);
var_dump($cantantes);
echo '<hr>';

//eliminar elemento específico del array
array_pop($cantantes);
unset($cantantes[2]);
var_dump($cantantes);
echo '<hr>';

//sacar un item aleatorio
$indice = array_rand($cantantes);
echo $cantantes[$indice];
echo '<hr>';

//Dar vuelta a un array
var_dump(array_reverse($numeros));
echo '<hr>';

//buscar en un array
$resultado = array_search('Anathema', $cantantes);
var_dump($resultado);
echo '<hr>';

//contar numero de elementos
echo count($cantantes);
echo '<hr>';

//lo mimso con sizeof
echo sizeof($cantantes);

