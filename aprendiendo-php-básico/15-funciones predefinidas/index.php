<?php

//debugear
$nombre = "José";
var_dump($nombre);
echo "<br/>";

//fechas
echo date('d-M-y');
echo "<br/>";

echo time();
echo "<br/>";

//matematicas
echo "raiz cuadrada de 10: ".sqrt(10);
echo "<br/>";

//Rand (genera un numero aleatorio, los limites e coloan en los parámetros)
echo "Número aleatoreo entre 1o y 40: ".rand(10,40);
echo "<br/>";

echo "Numero pi: ".pi();
echo "<br/>";

//metodo para redondear, round() acepta un segundo parámetro con la precisión.
//Laprecisión indica el número de decimales deceados.
echo "Redondear: ".round(7.789, 2);
echo "<br/>";
echo "<br/>";


//***********************************
//*********parte2 [clase 62]*********
//***********************************

echo gettype($nombre);
echo "<br/>";

//detectar tipado
if(is_string){
    echo "Esa variable es un string";
}
echo "<br/>";
if(!is_float($nombre)){
    echo "La variable nombre no es un numero con decimales";
}
echo "<br/>";
/*********************************************************** */
//la función isset comprueba si existe una variable

if(isset($edad)){ //probar con $nombre también
    echo "Lavariable existe";
}else {
    echo "la Variable no existe";
}
echo "<br/>";
/************************************************************* */
//La función trim limpia el contenido de la variable por delante y por detrás

$frase = "        mi contenido        ";
var_dump (trim($frase));
echo "<br/>";
/************************************************************** */

//eliminar variables / indices de arrays

$year = 2020;
unset($year);
var_dump($year);
echo "<br/>";
/******************************************************** */

//comprobar variable vacia
$texto = "eee";

if(empty($texto)){
    echo "La variable texto esta vacia";
}else{
    echo "la variable texto tiene contenido";
}
echo "<br/>";
/************************************************************* */

//contar caracteres de un string

$cadena = "12345";
echo strlen($cadena);
echo "<br/>";
/****************************************************************** */

//encotrar caracter
$frase = "La vida es bella";
echo strpos($frase, "vida");
echo "<br/>";
/************************************************************* */

// Reemplazar palabras de un string
$frase = str_replace("vida", "moto", $frase);
echo $frase;
echo "<br/>";
/******************************************************************** */

// Mayusculas y minusculas

echo strtolower($frase);
echo "<br/>";
echo strtoupper($frase);