<?php
/*
Para mostrar el valor de las cookies debo usar $_COOKIE,
es una variable superglobal, también es un array asociativo.
*/

if(isset($_COOKIE['micookie'])){
    echo "<h4>".$_COOKIE['micookie']."</h4>";
}else{
    echo "No existe la cookie";
}

echo "<hr>";

if(isset($_COOKIE['unyear'])){
    echo "<h4>".$_COOKIE['unyear']."</h4>";
}else{
    echo "No existe la cookie";
}

?>

<a href="crear_cookies.php">Crear mis galletas</a>
<a href="borrar_cookies.php">Borrar mis galletas</a>