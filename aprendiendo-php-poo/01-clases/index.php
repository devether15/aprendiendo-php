<?php

//programacion Orientada a Objetos en PHP (POO)

//Definir una clase

//Es un molde para crear más objetos del tipo Coche con caracteristicas parecidas

class Coche{
    //atributos o propiedades
    public $color = 'Rojo';
    public $modelo = 'Ferarri';
    public $marca = 'F350';
    public $velocidad = '300';
    public $caballaje = '500';
    public $plazas = '2';

    //metodos (antes funciones) Son funciones que hace el objeto, (su comportamiento)

    public function getColor(){
        //el $this busca en esta clase la propiedad indicada
        return $this->color;
    }

    public function setColor($color){
        $this->color = $color;
    }

    public function setModelo($modelo){
        $this->modelo = $modelo;
    }

    public function acelerar(){
        $this->velocidad++;
    }

    public function frenar(){
        $this->velocidad--;
    }

    public function getVelocidad(){
        return $this->velocidad;
    }

} // fin definicion de la clase

//crear un objeto ó instanciar la clase

$coche = new Coche();

//var_dump($coche);

//usar los métodos

echo $coche->getVelocidad().'<hr>';

$coche->setColor("Amarillo");

echo "El color del coche es: ".$coche->getColor().'<hr>';

$coche->acelerar().'<br>';
$coche->acelerar().'<br>';
$coche->acelerar().'<br>';
$coche->acelerar().'<br>';
$coche->frenar();

echo "velocidad del coche: ".$coche->getVelocidad().'<hr>';


//instanciamos otro objeto (se pueden tantos como queramos)

$coche2 = new Coche();
$coche2->setColor('verde');
$coche2->setModelo('Gallardo');

var_dump($coche);
var_dump($coche2);
