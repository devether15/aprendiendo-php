<?php

/*
Arrays
Un array es una colección o conjunto de datos, bajo un único nombre.
ara acceder a esos valores odemos usar un indice numerico o alfanumerico.
*/

//variable simple
$pelicula = "Batman";

//array numerico
$peliculas = array('Batman', 'Spiderman', 'El señor de los anillos');

//accecideo a un item unico del array
//var_dump($peliculas[1]);
echo $peliculas[0];
echo "<hr/>";

//forma de declarar un array:

$cantantes = ['2pac', 'Drake', 'J Lo'];
echo $cantantes[2];

//var_dump($cantantes);

/************************************** */
/****** Recorriendo un array ********** */
/************************************** */

//1) Recorriendo array de $peliculas con un for
echo "<h1>Listado de peliculas</h1>";

echo "<ul>";
for ($i = 0; $i < count($peliculas); $i++){
    echo"<li>".$peliculas[$i]."</li>";
}
echo "</ul>";

echo "<hr>";

//2) Recorriendo array de cantantes con un ForEach
echo "<h1>Listado de cantantes</h1>";

echo "<ul>";
foreach ($cantantes as $key => $cantante) {
    echo "<li>".$cantante."</li>";
}
echo "</ul>";

echo "<hr/>";

/************************************** */
/****** Arrays asociativos ************ */
/************************************** */

$personas = array(
    'nombre' => 'Victor',
    'apellido' => 'Robles',
    'web' => 'victorroblesweb.es'
);

//var_dump($personas['apellido']);

echo $personas['apellido'];
echo "<hr/>";

//los parametros $_GET son arrays asociativos
//http://localhost:8888/aprendiendo-php/aprendiendo-php-b%C3%A1sico/17-arrays/?hola=1&nombre=jose

var_dump($_GET);
echo "<hr/>";
/************************************** */
/****** Arrays multidimensionales ***** */
/************************************** */

$contactos = array (
    array(
        'nombre' => 'Antonio',
        'email' => 'antonio@gmail.com',
        'web' => 'kusiantonio.com'
    ),
    array(
        'nombre' => 'Luis',
        'email' => 'luis@gmail.com'
    ),
    array(
        'nombre' => 'Miguel',
        'email' => 'miguel@gmail.com'
    )
);

//var_dump($contactos);


//Accdeido al nombre Luis
echo $contactos[1]['nombre'];
echo '<br/>';
 
//Accediendo al email de Miguel
echo $contactos[2]['email'];
echo '<hr>';

//recorriendo un array multidimensional

foreach ($contactos as $key => $contacto) {
    //var_dump($contacto);
    var_dump($contacto['nombre']);
}
