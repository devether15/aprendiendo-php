<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>Imprimir por pantalla - Master en PHP</title>
    </head>
    <body>
        <h1>Master en <?php echo 'PHP'; ?></h1>
        <?= "Este es una sintaxis de impresión estilo echo"?>
        
        <?php
            //Esto es un comentario PHP
            echo "<h3>Listado de videojuegos:</h3>";
            //este es otro
            echo "<ul>"
                    . "<li>GTA</li>"
                    . "<li>FIFA</li>"
                    . "<li>Mario Bros</li>"
                . "</ul>";
            
            echo '<p>Esta es toda'.'- '.'lista de juegos</p>';
            
            /* 
             * Esto es un 
             * comentario
             * multilinea
             */
            
            
            //echo "<p>hola</p>"
        ?>
    </body>
</html>


