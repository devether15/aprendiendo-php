<?php
/*
Cookie: Es un fichero que se almacena en la computadoraa del usuario que 
visita la web, con el fin de recordar datos o rastrear el comportamiento 
del mismo en la web.

La diferencia con la sesión es que la cookie se almacena en el navegador
y es un poco más inseguro que la sesión porque el usuario
puede manipularlas
*/

//crear Cookies
//setcookie("nombre", "valor que solo puede ser texto", caducidad, ruta, dominio);

setcookie("micookie", "valor de mi galleta");

//Cookie con expiración
setcookie("unyear", "valor de mi cookie de 365 días", time()+(60*60*24*365));

header('Location:ver_cookies.php');

?>







