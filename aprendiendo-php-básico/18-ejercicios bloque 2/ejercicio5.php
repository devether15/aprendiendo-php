<?php
/*
Crear un array con el contenido de la siguiente tabla:

ACCION    AVENTURA    DEPORTES
gta       assasains   fifa 19    
COD       crash       pes 19
pugb      prince      moto gp 19
          of persia    

cada fila debe tener un fichero separado          
*/

$tabla = array (
    "ACCION" => array("GTA", "Call of Duty", "PUGB"),
    "AVENTURA" => array("Assasains creed", "Crash", "Prince of persia"),
    "DEPORTES" => array("FIFA 19", "PES 19", "Moto gp 19")
);

$categorias = array_keys($tabla);
?>

<table border="1">

   <?php require_once 'ejercicio5/encabezado.php' ?> 
   
   <?php include_once 'ejercicio5/row1.php' ?>

   <?php include_once 'ejercicio5/row2.php' ?> 

   <?php include_once 'ejercicio5/row3.php' ?> 
   
</table>