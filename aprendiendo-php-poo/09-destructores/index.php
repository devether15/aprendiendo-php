<?php

class usuario 
{
    public $nombre;
    public $email;
    
    public function __construct() 
    {
        $this->nombre = "Jose Arevalo";
        $this->email = "jose@gmail.com";
        echo "Creando el objeto <br/>";
    }

    public function __destruct()
    {
        echo "<br/>Destruyendo el objeto";
    }

    public function __toString()
    {
        return "hola, {$this->nombre}, estás registrado c on {$this->email}";
    }
}

$usuario = new Usuario;

echo $usuario;
