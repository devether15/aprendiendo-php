<?php
/*
Ejercico 1. Hacer un programa en php que tenga un array con 8 numeros enteros
y que haga lo siguiente:

- 1) recorrerlo y mostrarlo [Done]
- 2) Ordenarlo y mostrarlo
- 3) mostrar su longitud
- 4) buscar un elemeno
*/

$numeros = [4,5,6,7,8,1,2,3];

//función para mostrar array
function mostrarArray($numeros){
    $resultado = "";
    foreach($numeros as $numero){
         //$numero."<br/>";
        $resultado .=$numero."<br/>";
    }
    return $resultado;
}

//1
echo "<h2>Recorrer y mostrar</h2>";
echo mostrarArray($numeros);
echo '<hr>';

//2
echo "<h2>Ordenar y mostrarlo</h2>";
asort($numeros);
echo mostrarArray($numeros);
echo '<hr>';

//3
echo "<h2>Longitud</h2>";
echo count($numeros);
echo '<hr>';

//4
echo "<h2>Buscar en array</h2>";
$result = array_search(8, $numeros);
var_dump($result);
echo '<hr>';


//solución del punto 4 del instructor:
$busqueda = 55;

$search = array_search($busqueda, $numeros);

if(!empy($search)){
    echo "<h4>El numero existe en el array, en el indice :$search</h4>";
}else{
    echo "<h4>No existe el número buscado</h4>";
}

