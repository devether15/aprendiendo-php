<?php

/*
Crear script que tenga 4 variables, una de tipo array, otra de tipo string,
otra int, y otra booleana y que imprima un mensaje según el tipo de 
variable que sea.

*/ 

//mi solución:

$cars = array("toyota", "subaru", "honda", "susuki", "mitsubichi");
$store = "japan cars";
$stock = 34;
$available = true;

echo "<hr>";

if(is_array($cars)){
    echo "<h4>El array es un array</h4>";
}

if(is_string($store)){
    echo "<h4>$store</h4>";
}

if (is_int($stock)){
    echo "<h4>$stock</h4>";
}

if(is_bool($available)){
    echo "<h4>El boolean es true</h4>";
}