<?php

/* 
Ejercicio 3
 
 Escribir un script que imprima por pantalla los cuadrados
 (un numero multiplicado por sí mismo) de los 40 primeros número naturales
 
 PD: utilizar el bucle while
 
 */
$contador = 1;
while ($contador <= 40){
    $cuadrado = $contador*$contador;
    echo "El cuaadrado el $contador es $cuadrado <br/>";
    
    $contador++;
}


?>