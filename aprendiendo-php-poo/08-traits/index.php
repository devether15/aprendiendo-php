<?php 
//un trait permite delcrara metodos, que pueden ser usado por las clases pero sin tener herencia.
trait Utilidades {
    public function mostarNombre(){
        echo "<h1>El nombre es ".$this->nombre."</h1>";
    }
}

class Coche {
    public $nombre;
    public $marca;

    use Utilidades;
}

class Persona {
    public $nombre;
    public $apellido;

    use Utilidades;
}

class VideoJuego {
    public $nombre;
    public $anio;

    use Utilidades;
}

$coche = new Coche;
$persona = new Persona;
$videojuego = new VideoJuego; 

var_dump($coche);
echo '<hr>';

$coche->nombre = "Subaru";
$coche->mostarNombre();

$persona->nombre = "Pablo";
$persona->mostarNombre();

$videojuego->nombre = "Mario Bross";
$videojuego->mostarNombre();
