<?php
/*
//EJERCICIO 1 (opteniendoel parametro con $_GET)

function tabla  ($numero){
    echo "<h3> Tabla de multiplicar del numero : $numero </h3>";

    for($i = 1; $i <= 10; $i++){
        $operacion =$numero * $i;

        echo "$numero x $i = $operacion <br/>";
    }
}

if(isset($_GET['numero'])){
    tabla($_GET['numero']);
}else{
    echo "NO hay un numero ingresado para calcular la tabla";
}

// forma de pasar un parámetro por la URL con $_GET
//http://localhost:8888/master-php/aprendiendo-php/12-funciones/?numero=8

for ($i = 0; $i <= 10; $i++){
    tabla($i);
}
*/

//EJERCICIO 2 (parametros opcionales)

// function calculadora($numero1, $numero2, $negrita = false){
//     $suma = $numero1 + $numero2;
//     $resta = $numero1 - $numero2;
//     $multi = $numero1 * $numero2;
//     $division = $numero1 / $numero2;

//     if($negrita){
//         echo "<h1>";
//     }

//     echo "Suma: $suma <br/>";
//     echo "Resta: $resta <br/>";
//     echo "Multiplicaicon: $multi <br/>";
//     echo "Dvision : $division <br/>";
//     echo "<hr/>";

//     if($negrita){
//         echo "</h1>";
//     }
// }

// calculadora(10, 30);
// calculadora(12, 55, true);
// calculadora(15, 32);


//EJERCICIO 3  (rerturn)

function calculadora($numero1, $numero2, $negrita = false){
    $suma = $numero1 + $numero2;
    $resta = $numero1 - $numero2;
    $multi = $numero1 * $numero2;
    $division = $numero1 / $numero2;

    $cadena_texto = "";

    if($negrita){
        $cadena_texto .= "<h1>";
    }

    $cadena_texto .= "Suma: $suma <br/>";
    $cadena_texto .= "Resta: $resta <br/>";
    $cadena_texto .= "Multiplicaicon: $multi <br/>";
    $cadena_texto .= "Dvision : $division <br/>";
   

    if($negrita){
        $cadena_texto .= "</h1>";
    }

    $cadena_texto .= "<hr/>";

    return $cadena_texto;
}

echo calculadora(10, 30);
echo calculadora(12, 15, true);
echo calculadora(25, 14);


//EJEMPLO CONCEPTUAL DEL USO DEL RETURN
//se debería usar para regresar algo desde adentro 
//de la función, y luego imprimirla invocandola fuera
//de esta

// function devuelveELnombre ($nombre){
//     return "El nombre es: $nombre";
// }

// echo devuelveELnombre("José Arévalo");