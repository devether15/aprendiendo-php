<?php

// una interfaces es un contrato, además cuando heredemos
//debe cumplir el contrato al pid ede la letra

interface Ordenador {
    public function encender();
    public function apagar();
    public function reiniciar();
    public function desfragmentar();
    public function detectarUSB();
}

class iMac implements Ordenador {
    public $modelo;

    function getModelo(){
        return $this->modelo;
    }

    function setModelo($modelo){
        $this->modelo = $modelo;
    }
//los siguientes métdos deben estar si o si delcarados cuando implemento una interface
//para que se cumpla el contrato y la clase pueda funcionar.
    public function encender(){

    }

    public function apagar(){

    }

    public function reiniciar(){

    }

    public function desfragmentar(){

    }

    public function detectarUSB(){
        
    }
}

$macintosh = new iMac();
$macintosh->setModelo('MacBook Air 2015');
echo $macintosh->getModelo();

//var_dump($macintosh);