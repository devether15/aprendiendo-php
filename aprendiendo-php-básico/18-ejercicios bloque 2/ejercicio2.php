<?php

/*
Escribir un programa con PHP que añada valores a un array mientras que su longitud
sea menor que 120 y luego mostrarlo por pantalla.
*/

//mi solución

$myArray = [];
//var_dump ($myArray);

// function addToArray($newItem){
//     $resultado = "";
//     if(count(myArray) <= 120){
//         $resultado .=array_push($myArray, $newItem);
//     }else{
//         echo "<h4>El array está lleno</h4>";
//     }
//     return $resultado;
// }

// echo addToArray(3);

//solución del instructor
$coleccion = array();

for ($i = 0; $i < 120; $i++){
    array_push($coleccion,"elemento-".$i);
}

var_dump($coleccion);
echo "<hr>";
echo $coleccion[45];
