<?php

/*
 //CONDICIONAL IF
 if(condicion){
    instruciones
 }else{
    otras instrucciones
 }
 
 
 //Operadores de comparación
 
 == igual
 === identico
 != distinto
 <> diferente
 !== no identico
 < menor que
 > mayor que
 <= menor o igual que
 >= mayor o igual que
 
 //Operadores lógicos
 && AND (Y)
 || OR (ó)
 ! NOT (no)
 and or
*/

//EJEMPLO 1

//$color = "rojo";
$color = "verde";

if($color == 'rojo'){
    echo "el color es rojo";
}else
    echo "el color no es rojo";

//EJEMPLO 2
echo "<br>";

$year = 2018;

if($year >= 2019){
    echo "Estamos en 2019 en adelante";
} else {
    echo "Es un año anterior a 2019";
}

//ejemplo 3
$nombre = "Elvis Gil";
$ciudad = "madrid";
//$continente = "Europa";
$continente = "Asia";
$edad = 49;
$mayoria_edad = 18;

if($edad  >= $mayoria_edad){
    echo "<h1>$nombre es de mayor edad</h1>";
    
    if($continente == "Europa") {
        echo "<h2> y es de $ciudad</h2>";
    }else{
        echo "no es europeo";
    }
    
}else {
    echo "<h2> $nombre no es mayor de edad";
}
echo "<hr>";

//Ejemplo 4
$dia = 7;
/*
if($dia == 1){
    echo "Es lunes";
} else {
    if($dia == 2){
        echo "Es martes";
    } else{
        if($dia == 3){
            echo "Es miercoles";
        }else {
            if($dia == 4){
                echo "Es jueves";
        } else {
             if ($dia == 5) {
                    echo "Es viernes";
             } else {
                    echo "Es fin de semana";
             }
    
            }
        }
    }
}
*/

if ($dia == 1) {
    echo "Lunes";
}elseif ($dia == 2) {
    echo "Martes";
}elseif ($dia == 3) {
    echo "Miércoles";
}elseif ($dia == 4) {
    echo "Jueves";
}elseif ($dia == 5) {
    echo "Viernes";
} else {
    echo "Es fin de semana";
}
echo "<hr>";

//SWITCH
$dia = 45;

switch ($dia) {
    case 1:
        echo "lunes";
        break;
    case 2:
        echo "martes";
        break;
    case 3:
        echo "miercoles";
        break;
    case 4:
        echo "jueves";
        break;
    case 5:
        echo "viernes";
        break;
    default:
        echo "es fin de semana";
}

echo "<hr>";


//Ejemplo 5
$edad1 = 18;
$edad2 = 64;
$edad_oficial = 17;

if ($edad_oficial >= $edad1 && $edad_oficial <= $edad2) {
    echo "Está en edad de trabajar";
} else {
    echo "no puede trabajar";
}
echo "<hr>";
 
//otro ejemplo

$pais = "Francia";
if ($pais == "Mexico" || $pais == "España" || $pais == "Cololmbia") {
    echo "En este país se llama español";
}else{
    echo "no se habla español";
}
echo "<hr>";


//Go to
goto marca;
echo '<h3>Instrucción 1</h3>';
echo '<h3>Instrucción 2</h3>';
echo '<h3>Instrucción 3</h3>';
echo '<h3>Instrucción 4</h3>';

$pais = "Francia";
if ($pais == "Mexico" || $pais == "España" || $pais == "Cololmbia") {
    echo "En este país se llama español";
}else{
    echo "no se habla español";
}
echo "<hr>";

marca:
echo "<h1>Me he saltado 4 echos</h1>"
?>

