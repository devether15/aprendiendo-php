<! DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Formularios PHP y HTML</title>
    </head>
    <body>
        <h1>Formulario</h1>
        <form action="" method="POST" enctype="multipart/form-data">
           <label for="nombre">Nombre: </label> 
            <p><input type="text" name="nombre" minlength="5"/><br/></p>

            <label for="apellido">Apellido:</label> 
            <p><input type="text" name="apellido" autofocus="autofocus" required="required"/><br/></p>
            
            <label for="email">Email:</label> 
            <p><input type="text" name="email" autofocus="autofocus" required="required" placeholder="Ingrese su correo"/><br/></p>

            <label for="apellido">Apellido:</label> 
            <p><input type="text" name="apellido" autofocus="autofocus" required="required"/><br/></p>

            <label for="sexo">Sexo:</label> 
            <p>
            Hombre <input type="checkbox" name="sexo" value="Hombre"/>
            Mujer <input type="checkbox" name="sexo" value="Mujer"/>
            </p>

            <label for="color">Color:</label> 
            <p><input type="color" name="color"/><br/></p>

            <label for="fecha">fecha:</label> 
            <p><input type="date" name="fecha" /><br/></p>

            <label for="Archivo">Archivo:</label> 
            <p><input type="file" name="archivo" multiple="multiple " /><br/></p>

            <label for="numero">Numero:</label> 
            <p><input type="number" name="numero"/><br/></p>

            <label for="pass">Pass:</label> 
            <p><input type="password" name="pass" /><br/></p>

            <label for="continente">Continente:</label> 
            <p>America del sur<input type="radio" name="continente" value="America del sur " /></p>
            <p>Europa <input type="radio" name="continente" value="Europa " /></p>
            <p>Asia<input type="radio" name="continente" value="Asia" /></p>

            <label for="web">Pagina web:</label> 
            <p><input type="url" name="web" /><br/></p>

            <textarea></textarea><br/>

            Peliculas:<br/>
            <select name="pelicula">
                <option value="terminator">Terminator</option>
                <option value="titanic">Titanic</option>
                <option value="avatar">Avatar</option>
                <option value="terminator-2">Terminator 2</option>
            </select>
            <br/>


            <input type="submit" value="Enviar" />
        </form>
    </body>
</html>

<!-- Otros atributos para los inputs: minlength="5", disabled="disabled" -->