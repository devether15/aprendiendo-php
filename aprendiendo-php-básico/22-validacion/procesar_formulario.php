<?php
$error = 'faltan_valores';

if(!empty($_POST['nombre']) && !empty($_POST['apellidos']) && 
    !empty($_POST['edad']) && !empty($_POST['email']) && !empty($_POST['pass'])){
        $error = 'ok';

        $nombre = $_POST['nombre'];
        $apellidos = $_POST['apellidos'];
        $edad = $_POST['edad'];
        $email = $_POST['email'];
        $pass = $_POST['pass'];

        //validar nombre
        if(is_string($nombre) && !preg_match("/[a-zA-Z ]+/", $nombre)){
            $error = 'nombre';            
        }

        //validar apellidos
        if(is_string($apellidos) && !preg_match("/[a-zA-Z ]+/", $apellidos)){
            $error = 'apellidos';            
        }

        //validar edad
        if(is_number($edad) && !filter_var($edad, FILTER_VALIDATE_INT)){
            $error = 'edad';            
        }

        //validar email
        if(is_string($email) && !filter_var($edad, FILTER_VALIDATE_EMAIL)){
            $error = 'email';            
        }

        //validar pass
        if(empty($pass) && strlen($pass)<5){
            $error = 'pass';            
        }

}else{
    $error = 'faltan_valores';    
}

if ($error != 'ok') {
    header("Location:index.php?error=$error");
}
?>


<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />    
        <title>Valdación de Formulario</title>    
    </head>
    <body>
    <h1>Datos validados correctamente</h1>
        <?php if($error == 'ok'): ?>
            <p><?=$nombre?></p>
            <p><?=$apellidos?></p>
            <p><?=$edad?></p>
            <p><?=$email?></p>
            <p><?=$pass?></p>
        <?php endif; ?>

    </body>
</html>