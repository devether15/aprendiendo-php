<?php

session_start();

//esta variablees local de otro ficheo, por tanto no se mostrará
echo $variable_normal;

//está fue declarada en una sesión, por tanto puedo usarla en varios ficheros
echo $_SESSION['variable_persistente'];

?>

<ul>
    <li><a href="index.php">Index</a></li>
    <li><a href="pagina1.php">Pagina1</a></li>
    <li><a href="logout.php">logout</a></li>
</ul>