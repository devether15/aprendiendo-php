<?php 

abstract class Ordenador {
    public $encendido;

    abstract public function encender();   
    
    public function apagar(){
        $this->encendio = false;
    }
}

class pcAsus extends Ordenador {
    public $software;

    public function arrancarSoftware(){
        $this->software = true;
    }

    public function encender(){
        $this->encendido =true;
    }
}

$ordenador = new pcAsus;
var_dump($ordenador);

/************************* */

echo '<hr>';

$ordenador->arrancarSoftware();
$ordenador->encender();
$ordenador->apagar();

var_dump($ordenador);
