# Aprendiendo-PHP

_Notas y ejercicios "hands on" de José Arévalo_ 

## Udemy 🚀

* **curso**: Master en PHP, SQL, POO, MVC, Laravel y Symfony
* **Instructor**: Victor Robles
* **Disponible en**: https://www.udemy.com/master-en-php-sql-poo-mvc-laravel-symfony-4-wordpress/learn/v4/

*"Un programador es un mamífero nocturno de ojos rojos capaz de conversar con objetos inanimados."*
